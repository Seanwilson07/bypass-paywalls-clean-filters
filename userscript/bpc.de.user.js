// ==UserScript==
// @name            Bypass Paywalls Clean - de/at/ch
// @version         2.7.6.3
// @downloadURL     https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.de.user.js
// @updateURL       https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.de.user.js
// @match           *://*.de/*
// @match           *://*.at/*
// @match           *://*.ch/*
// ==/UserScript==

(function() {
  'use strict';

window.setTimeout(function () {

var de_funke_media_domains = ['abendblatt.de', 'braunschweiger-zeitung.de', 'morgenpost.de', 'nrz.de', 'otz.de', 'thueringer-allgemeine.de', 'tlz.de', 'waz.de', 'wp.de', 'wr.de'];
var de_madsack_domains = ['haz.de', 'kn-online.de', 'ln-online.de', 'lvz.de', 'maz-online.de', 'neuepresse.de', 'ostsee-zeitung.de', 'rnd.de'];
var de_madsack_custom_domains = ['aller-zeitung.de', 'dnn.de', 'gnz.de', 'goettinger-tageblatt.de', 'paz-online.de', 'sn-online.de', 'waz-online.de'];
var domain;

if (matchDomain('allgaeuer-zeitung.de')) {
  let url = window.location.href;
  if (!url.includes('?type=amp')) {
    let paywall = document.querySelector('p.nfy-text-blur');
    if (paywall) {
      removeDOMElement(paywall);
      window.location.href = url.split('?')[0] + '?type=amp';
    }
  } else {
    let preview = document.querySelectorAll('p.nfy-text-blur, div[subscriptions-display^="NOT data."]');
    let amp_ads = document.querySelectorAll('amp-ad');
    removeDOMElement(...preview, ...amp_ads);
  }
}

else if (matchDomain('augsburger-allgemeine.de')) {
  let url = window.location.href;
  if (!url.includes('-amp.html')) {
    let paywall = document.querySelector('div.aa-visible-logged-out');
    if (paywall) {
      removeDOMElement(paywall);
      window.location.href = url.replace('.html', '-amp.html');
    }
  } else {
    amp_unhide_subscr_section();
  }
}

else if (matchDomain('automobilwoche.de')) {
  let lazy_images = document.querySelectorAll('img.lazy[data-src]');
  for (let lazy_image of lazy_images) {
    lazy_image.src = lazy_image.getAttribute('data-src');
    lazy_image.removeAttribute('class');
  }
  let lazy_sources = document.querySelectorAll('source[srcset^="data:image"]');
  removeDOMElement(...lazy_sources);
}

else if (matchDomain('berliner-zeitung.de')) {
  let url = window.location.href;
  let paywall = document.querySelector('.paywall-dialog-box');
  if (url.split('?')[0].includes('.amp')) {
    if (paywall) {
      removeDOMElement(paywall);
      amp_unhide_subscr_section('amp-ad, amp-embed, amp-fx-flying-carpet, div.amp-flying-carpet-text-border');
    }
  } else {
    if (paywall) {
      removeDOMElement(paywall);
      window.location.href = url.split('?')[0] + '.amp';
    }
  }
}

else if (matchDomain('cicero.de')) {
  let url = window.location.href;
  if (!window.location.search.match(/(\?|&)amp/)) {
    let paywall = document.querySelector('.plenigo-paywall');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    }
  } else {
    let teasered_content = document.querySelector('.teasered-content');
    if (teasered_content)
      teasered_content.classList.remove('teasered-content');
    let teasered_content_fader = document.querySelector('.teasered-content-fader');
    let btn_read_more = document.querySelector('.btn--read-more');
    let amp_ads = document.querySelectorAll('amp-ad');
    removeDOMElement(teasered_content_fader, btn_read_more, ...amp_ads);
  }
  let urban_ad_sign = document.querySelectorAll('.urban-ad-sign');
  removeDOMElement(...urban_ad_sign);
}

else if (matchDomain(de_funke_media_domains)) {
  if (window.location.search.startsWith('?service=amp'))
    amp_unhide_access_hide('="NOT p.showRegWall AND NOT p.showPayWall"', '', 'amp-ad, amp-embed, amp-fx-flying-carpet');
  else
    sessionStorage.setItem('deobfuscate', 'true');
}

else if (matchDomain('freiepresse.de')) {
  let url = window.location.href;
  let article_teaser = document.querySelector('div.article-teaser');
  if (article_teaser && url.match(/(\-artikel)(\d){6,}/)) {
    window.setTimeout(function () {
      window.location.href = url.replace('-artikel', '-amp');
    }, 500);
  } else if (url.match(/(\-amp)(\d){6,}/)) {
    let amp_ads = document.querySelectorAll('amp-fx-flying-carpet, amp-ad, amp-embed');
    let pw_layer = document.querySelector('.pw-layer');
    removeDOMElement(...amp_ads, pw_layer);
  }
}

else if (matchDomain('krautreporter.de')) {
  let paywall = document.querySelector('.js-article-paywall');
  if (paywall) {
    removeDOMElement(paywall);
    window.setTimeout(function () {
      let paywall_divider = document.querySelector('.js-paywall-divider');
      let steady_checkout = document.querySelector('#steady-checkout');
      removeDOMElement(paywall_divider, steady_checkout);
      let blurred = document.querySelectorAll('.blurred');
      for (let elem of blurred)
        elem.classList.remove('blurred', 'json-ld-paywall-marker', 'hidden@print');
    }, 500);
  }
}

else if (matchDomain(['ksta.de', 'rundschau-online.de'])) {
  let paywall = document.querySelector('div#c1-template-platzhalter');
  if (paywall) {
    removeDOMElement(paywall);
    let span_hidden = document.querySelector('span.hide-paid-content');
    if (span_hidden)
      span_hidden.removeAttribute('class');
  }
}

else if (matchDomain('kurier.at')) {
  let view_offer = document.querySelector('.view-offer');
  removeDOMElement(view_offer);
  let plus_content = document.querySelector('.plusContent');
  if (plus_content)
    plus_content.classList.remove('plusContent');
}

else if (matchDomain(['mz.de', 'volksstimme.de'])) {
  let url = window.location.href.split('?')[0];
  let paywall = document.querySelector('.fp-paywall');
  if (url.includes('/amp/')) {
    amp_unhide_subscr_section('amp-ad, amp-embed');
  } else {
    if (paywall) {
      removeDOMElement(paywall);
      window.location.href = window.location.href.replace('.de/', '.de/amp/');
    }
  }
}

else if (matchDomain('nn.de')) {
  let ad_billboard = document.querySelector('div#ad-Billboard');
  if (ad_billboard && ad_billboard.parentNode)
    removeDOMElement(ad_billboard.parentNode);
  let ads = document.querySelectorAll('div[id^="clsdiv_"]');
  removeDOMElement(...ads);
}

else if (matchDomain(['noz.de', 'nwzonline.de', 'shz.de', 'svz.de'])) {
  if (window.location.pathname.match(/(-amp\.html|\/amp)$/)) {
    amp_unhide_access_hide('="NOT data.reduced"', '="data.reduced"', 'amp-ad, amp-embed, .ads-wrapper, #flying-carpet-wrapper');
  } else {
    let paywall = document.querySelector('.paywall, .story--premium__container');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    }
  }
}

else if (matchDomain('nzz.ch')) {
  if (!window.location.href.includes('/amp/')) {
    let paywall = document.querySelector('.dynamic-regwall');
    let amphtml = document.querySelector('link[rel="amphtml"]');
    if (paywall && amphtml) {
      removeDOMElement(paywall);
      window.location.href = amphtml.href;
    }
  } else {
    let amp_ads = document.querySelectorAll('amp-ad');
    removeDOMElement(...amp_ads);
  }
}

else if (matchDomain(['westfalen-blatt.de', 'wn.de', 'muensterschezeitung.de'])) {
  let url = window.location.href;
  if (url.includes('/amp/')) {
    amp_unhide_subscr_section('amp-ad, amp-embed, section[class^="fp-ad"]');
  } else {
    let paywall = document.querySelector('.fp-article-paywall-dialog, .fp-article-paywall');
    if (paywall) {
      removeDOMElement(paywall);
      let gallery_no_amp = false;
      if (matchDomain('westfalen-blatt.de')) {
        let article_body = document.querySelector('.fp-article__body');
        gallery_no_amp = url.includes('/fotos/') || (!article_body && document.querySelector('.fp-gallery-carousel'));
      }
      if (!gallery_no_amp)
        window.location.href = url.replace('.de/', '.de/amp/');
    }
  }
}

else if (matchDomain(de_madsack_domains) || matchDomain(de_madsack_custom_domains)) {
  if (!(window.location.pathname.startsWith('/amp/') || window.location.search.startsWith('?outputType=valid_amp'))) {
    let paidcontent_intro = document.querySelector('div.pdb-article-body-paidcontentintro');
    if (paidcontent_intro) {
      paidcontent_intro.classList.remove('pdb-article-body-paidcontentintro');
      let json_script = getArticleJsonScript();
      if (json_script) {
        let json_text = JSON.parse(json_script.text).articleBody;
        if (json_text) {
          let pdb_richtext_field = document.querySelectorAll('div.pdb-richtext-field');
          if (pdb_richtext_field[1])
            pdb_richtext_field[1].innerText = json_text;
        }
      }
      let paidcontent_reg = document.querySelector('div.pdb-article-paidcontent-registration');
      removeDOMElement(paidcontent_reg);
    } else {
      let paywall = document.querySelector('div.paywall');
      if (paywall) {
        let gradient = document.querySelector('div[class^="ArticleContentLoaderstyled__Gradient"]');
        let loading = document.querySelector('#article > svg');
        removeDOMElement(paywall, gradient, loading);
        let article = document.querySelector('div[class*="ArticleTeaserContainer"] > div:not([class])');
        let json_script = getArticleJsonScript();
        if (json_script) {
          let json = JSON.parse(json_script.text);
          if (article && json) {
            let json_text = json.articleBody;
            let article_new = document.createElement('span');
            let par = article.querySelector('p');
            let par_class = par ? par.getAttribute('class') : '';
            article_new.setAttribute('class', par_class);
            article_new.innerText = json_text;
            article.innerText = '';
            if (json.articleSection) {
              let json_section = json.articleSection;
              let article_section = document.querySelector('span');
              article_section.setAttribute('class', par_class);
              article_section.setAttribute('style', 'font-weight: bold;');
              article_section.innerText = json_section + '. ';
              article.appendChild(article_section);
            }
            article.appendChild(article_new);
          }
        }
      }
    }
  } else if (window.location.pathname.startsWith('/amp/')) {
    amp_unhide_subscr_section('.pdb-ad-container, amp-embed');
  }
}

}, 1000);

// General Functions

function matchDomain(domains, hostname) {
  var matched_domain = false;
  if (!hostname)
    hostname = window.location.hostname;
  if (typeof domains === 'string')
    domains = [domains];
  domains.some(domain => (hostname === domain || hostname.endsWith('.' + domain)) && (matched_domain = domain));
  return matched_domain;
}

function setCookie(name, value, domain, path, days) {
  window.localStorage.clear();
  var max_age = days * 24 * 60 * 60;
  document.cookie = name + "=" + (value || "") + "; domain=" + domain + "; path=" + path + "; max-age=" + max_age;
}

function removeDOMElement(...elements) {
  for (let element of elements) {
    if (element)
      element.remove();
  }
}

function waitDOMElement(selector, tagName = '', callback, multiple = false) {
  new window.MutationObserver(function (mutations) {
    for (let mutation of mutations) {
      for (let node of mutation.addedNodes) {
        if (!tagName || (node.tagName === tagName)) {
          if (node.matches(selector)) {
            callback(node);
            if (!multiple)
              this.disconnect();
          }
        }
      }
    }
  }).observe(document, {
    subtree: true,
    childList: true
  });
}

function amp_iframes_replace(weblink = false, source = '') {
  let amp_iframes = document.querySelectorAll('amp-iframe' + (source ? '[src*="'+ source + '"]' : ''));
  let elem;
  for (let amp_iframe of amp_iframes) {
    if (!weblink) {
      elem = document.createElement('iframe');
      Object.assign(elem, {
        src: amp_iframe.getAttribute('src'),
        sandbox: amp_iframe.getAttribute('sandbox'),
        height: amp_iframe.getAttribute('height'),
        width: 'auto',
        style: 'border: 0px;'
      });
      amp_iframe.parentElement.insertBefore(elem, amp_iframe);
      removeDOMElement(amp_iframe);
    } else {
      let video_link = document.querySelector('a#bpc_video_link');
      if (!video_link) {
        amp_iframe.removeAttribute('class');
        elem = document.createElement('a');
        elem.id = 'bpc_video_link';
        elem.innerText = 'Video-link';
        elem.setAttribute('href', amp_iframe.getAttribute('src'));
        elem.setAttribute('target', '_blank');
        amp_iframe.parentElement.insertBefore(elem, amp_iframe);
      }
    }
  }
}

function amp_unhide_subscr_section(amp_ads_sel = 'amp-ad, .ad', replace_iframes = true, amp_iframe_link = false, source = '') {
  let preview = document.querySelector('[subscriptions-section="content-not-granted"]');
  removeDOMElement(preview);
  let subscr_section = document.querySelectorAll('[subscriptions-section="content"]');
  for (let elem of subscr_section)
    elem.removeAttribute('subscriptions-section');
  let amp_ads = document.querySelectorAll(amp_ads_sel);
  removeDOMElement(...amp_ads);
  if (replace_iframes)
    amp_iframes_replace(amp_iframe_link, source);
}

function amp_unhide_access_hide(amp_access = '', amp_access_not = '', amp_ads_sel = 'amp-ad, .ad', replace_iframes = true, amp_iframe_link = false, source = '') {
  let access_hide = document.querySelectorAll('[amp-access' + amp_access + '][amp-access-hide]:not([amp-access="error"], [amp-access^="message"])');
  for (let elem of access_hide)
    elem.removeAttribute('amp-access-hide');
  if (amp_access_not) {
    let amp_access_not_dom = document.querySelectorAll('[amp-access' + amp_access_not + ']');
    removeDOMElement(...amp_access_not_dom);
  }
  let amp_ads = document.querySelectorAll(amp_ads_sel);
  removeDOMElement(...amp_ads);
  if (replace_iframes)
    amp_iframes_replace(amp_iframe_link, source);
}

function parseHtmlEntities(encodedString) {
  let translate_re = /&(nbsp|amp|quot|lt|gt|deg|hellip|laquo|raquo|ldquo|rdquo|lsquo|rsquo|mdash);/g;
  let translate = {"nbsp": " ", "amp": "&", "quot": "\"", "lt": "<", "gt": ">", "deg": "°", "hellip": "…",
      "laquo": "«", "raquo": "»", "ldquo": "“", "rdquo": "”", "lsquo": "‘", "rsquo": "’", "mdash": "—"};
  return encodedString.replace(translate_re, function (match, entity) {
      return translate[entity];
  }).replace(/&#(\d+);/gi, function (match, numStr) {
      let num = parseInt(numStr, 10);
      return String.fromCharCode(num);
  });
}

function encode_utf8(str) {
  return unescape(encodeURIComponent(str));
}

function decode_utf8(str) {
  return decodeURIComponent(escape(str));
}

function getArticleJsonScript() {
  let scripts = document.querySelectorAll('script[type="application/ld+json"]');
  let json_script;
  for (let script of scripts) {
    if (script.innerText.includes('articleBody')) {
      json_script = script;
      break;
    }
  }
  return json_script;
}

function breakText(str) {
  str = str.replace(/(?:^|[A-Za-z\"\“])(\.|\?|!)(?=[A-ZÖÜ\„\d][A-Za-zÀ-ÿ\„\d]{1,})/gm, "$&\n\n");
  str = str.replace(/(([a-z]{2,}|[\"\“]))(?=[A-Z](?=[A-Za-zÀ-ÿ]+))/gm, "$&\n\n");
  // exceptions: names with alternating lower/uppercase (no general fix)
  let str_rep_arr = ['AstraZeneca', 'BaFin', 'BerlHG', 'BfArM', 'BilMoG', 'BioNTech', 'DiGA', 'EuGH', 'FinTechRat', 'GlaxoSmithKline', 'IfSG', 'medRxiv', 'mmHg', 'PlosOne', 'StVO'];
  let str_rep_split,
  str_rep_src;
  for (let str_rep of str_rep_arr) {
    str_rep_split = str_rep.split(/([a-z]+)(?=[A-Z](?=[A-Za-z]+))/);
    str_rep_src = str_rep_split.reduce(function (accumulator, currentValue) {
        return accumulator + currentValue + ((currentValue !== currentValue.toUpperCase()) ? '\n\n' : '');
      });
    if (str_rep_src.endsWith('\n\n'))
      str_rep_src = str_rep_src.slice(0, -2);
    str = str.replace(new RegExp(str_rep_src, "g"), str_rep);
  }
  str = str.replace(/De\n\n([A-Z])/g, "De$1");
  str = str.replace(/La\n\n([A-Z])/g, "La$1");
  str = str.replace(/Le\n\n([A-Z])/g, "Le$1");
  str = str.replace(/Mc\n\n([A-Z])/g, "Mc$1");
  return str;
};

})();
